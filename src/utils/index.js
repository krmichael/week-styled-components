import { Linking, PermissionsAndroid } from 'react-native';

export function wait(time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}

export async function verifyCanOpenUrl(url) {
  // Verificando se o usuário possui algum app (browser, etc) instalado para abrir a url
  await Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        console.log(`A url > ${url} não pode ser aberta`);
      }

      return Linking.openURL(url);
    })
    .catch((error) => {
      console.log(error);
      throw new Error(
        'Houve um erro inesperado, por favor tente novamente mais tarde!',
      );
    });
}

export async function checkPermission() {
  try {
    const permited = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.CAMERA,
    );

    if (!permited) {
      return console.log('Você não concedeu permissão para acessar a camera');
    }

    console.log('Aproveite o App');
  } catch (error) {
    console.warn(error);
  }
}

export async function requestCameraPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: 'Cool Photo App Camera Permission',
        message:
          'Cool Photo App needs access to your camera ' +
          'so you can take awesome pictures.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );

    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the camera');
    } else {
      if (granted === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        console.log('NEVER_ASK_AGAIN');
      } else {
        console.log('Camera permission denied');
      }
    }
  } catch (error) {
    console.warn(error);
  }
}
