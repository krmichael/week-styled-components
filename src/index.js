import React from 'react';
import { StatusBar } from 'react-native';

import './config/ReactotronConfig';

import Transform from './components/Transform';

export default function App() {
  return (
    <>
      <StatusBar
        backgroundColor="#f2f2f2"
        barStyle="dark-content"
        translucent={true}
      />
      <Transform />
    </>
  );
}
