import React from 'react';
import { Share } from 'react-native';

import RippleButton from '../RippleButton';
import { Container } from './styles';

async function onShare() {
  try {
    const result = await Share.share({
      message:
        'Lean Silva | Um maluco que vive codando [24x7] - https://gitlab.com/leansilva',
    });

    if (result.action === Share.sharedAction) {
      console.log('shared');
    }

    if (result.action === Share.dismissedAction) {
      console.log('dismiss');
    }
  } catch (error) {
    console.warn(error);
  }
}

export default function ShareComponent() {
  return (
    <Container>
      <RippleButton label="Share" onPress={onShare} />
    </Container>
  );
}
