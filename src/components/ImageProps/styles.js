import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #f2f2f2;
  justify-content: center;
  align-items: center;
`;

export const Avatar = styled.Image.attrs((props) => ({
  width: props.width,
  height: props.height,
  borderRadius: props.borderRadius,
}))`
  width: ${(props) => props.width || 200}px;
  height: ${(props) => props.height || 200}px;
  border-radius: ${(props) => props.borderRadius || 200}px;
`;

export const Text = styled.Text`
  color: palevioletred;
  font-size: 18px;
  font-weight: bold;
`;
