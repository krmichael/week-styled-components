import React from 'react';

import avatarUser from '../../assets/avatar.png';
import { Container, Avatar } from './styles';

export default function ImageProps() {
  return (
    <Container>
      <Avatar source={avatarUser} resizeMode="contain" />
    </Container>
  );
}
