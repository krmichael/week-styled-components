import React, { useEffect } from 'react';
import { Animated } from 'react-native';

import { Container, Box } from './styles';

// @refresh reset
export default function TransformComponent() {
  const translateX = new Animated.Value(0);
  const translateY = new Animated.Value(0);
  const rotate = new Animated.Value('0deg');

  useEffect(() => {
    Animated.parallel([
      Animated.timing(translateX, {
        toValue: 10,
      }),
      Animated.timing(translateY, {
        toValue: 5,
      }),
      Animated.timing(rotate, {
        toValue: '10deg',
      }),
    ]).start();
  }, [rotate, translateX, translateY]);

  return (
    <Container>
      <Box
        style={{
          transform: [{ translateX: translateY }],
        }}
      />
    </Container>
  );
}
