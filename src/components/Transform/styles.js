import { Animated } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #f2f2f2;
`;

export const Box = styled(Animated.View)`
  width: 200px;
  height: 200px;
  border: 2px solid #fff;
  background-color: paleturquoise;
`;
