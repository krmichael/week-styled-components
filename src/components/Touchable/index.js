import React from 'react';

import Container from './styles';
import Highlight from './Highlight';
import NativeFeedBack from './NativeFeedBack';
import WithoutFeedback from './WithoutFeedback';

export default function App() {
  return (
    <Container>
      <Highlight />
      <NativeFeedBack />
      <WithoutFeedback />
    </Container>
  );
}
