import React from 'react';
import { TouchableNativeFeedback, Platform } from 'react-native';

import { ScopeButton, Button, ButtonView, ButtonText } from './styles';

export default class NativeFeedBack extends React.Component {
  render() {
    return (
      <ScopeButton>
        <Button
          background={
            Platform.Version >= 21
              ? TouchableNativeFeedback.Ripple('palevioletred')
              : TouchableNativeFeedback.SelectableBackground()
          }
          useForeground={TouchableNativeFeedback.canUseNativeForeground()}
          onPress={() =>
            console.log('Button with native feedback was pressed now!')
          }
          onPressOut={() => console.log('onPressOut')}
          delayLongPress={500}>
          <ButtonView>
            <ButtonText>Native FeedBack</ButtonText>
          </ButtonView>
        </Button>
      </ScopeButton>
    );
  }
}
