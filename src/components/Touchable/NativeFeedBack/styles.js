import styled from 'styled-components/native';

export const ScopeButton = styled.View`
  width: 250px;
  background-color: rebeccapurple;
  margin-top: 20px;
  height: 50px;
  justify-content: center;
  align-items: center;
  border-radius: 3px;
`;

export const Button = styled.TouchableNativeFeedback``;

export const ButtonView = styled.View`
  width: 250px;
  justify-content: center;
  align-items: center;
  height: 50px;
`;

export const ButtonText = styled.Text`
  color: white;
  font-size: 16px;
  font-weight: bold;
`;
