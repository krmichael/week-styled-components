import React from 'react';

import { ScopeButton, Button, ButtonText } from './styles';

export default function WithoutFeedback() {
  return (
    <ScopeButton>
      <Button
        onPress={() => console.log('Button without feedback was pressed now!')}>
        <ButtonText>without feedback</ButtonText>
      </Button>
    </ScopeButton>
  );
}
