import styled from 'styled-components/native';

export const ScopeButton = styled.View`
  margin-top: 20px;
  background-color: #0099ff;
  width: 250px;
  height: 50px;
  border-radius: 3px;
  justify-content: center;
  align-items: center;
`;

export const Button = styled.TouchableWithoutFeedback``;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 16px;
  font-weight: bold;
`;
