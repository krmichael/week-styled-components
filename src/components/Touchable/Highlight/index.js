import React from 'react';

import { Button, ButtonText } from './styles';

export default function App() {
  return (
    <Button
      underlayColor="palevioletred"
      activeOpacity={0.7}
      onHideUnderlay={() =>
        console.log('Underlay foi ocultado (Usuário soltou o botão)')
      }
      onShowUnderlay={() =>
        console.log('Underlay foi mostrado (Usuário pressionou o botão)')
      }
      onPressIn={() => console.log('Usuário pressiou o botão')}
      onPressOut={() => console.log('Usuário soltou o botão')}
      onLongPress={() =>
        console.log('Usuário esta pressionando o botão a muito tempo')
      }
      onPress={() => {}}>
      <ButtonText>Press me!</ButtonText>
    </Button>
  );
}
