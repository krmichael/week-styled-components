import styled from 'styled-components/native';

export const Button = styled.TouchableHighlight`
  background-color: palevioletred;
  width: 250px;
  height: 50px;
  border-radius: 3px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 16px;
  font-weight: bold;
`;
