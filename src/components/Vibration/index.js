import React, { useState } from 'react';
import { Vibration } from 'react-native';

import { Container } from './styles';
import RippleButton from '../RippleButton';

export default function VibrationComponent() {
  const [repeat, setRepeat] = useState(false);

  const DURATION = 1000;
  const PATERN = [1000, 2000, 3000];

  return (
    <Container>
      {!repeat && (
        <RippleButton
          label="Press to vibrate"
          onPress={() => Vibration.vibrate(DURATION)}
        />
      )}

      <RippleButton
        label="Press to vibrate loop"
        onPress={() => {
          setRepeat(!repeat);
          Vibration.vibrate(PATERN, true);
        }}
        disabled={repeat}
      />

      {repeat && (
        <RippleButton
          label="Press to cancel vibrate"
          onPress={() => {
            setRepeat(!repeat);
            Vibration.cancel();
          }}
        />
      )}
    </Container>
  );
}
