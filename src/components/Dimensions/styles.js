import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #f2f2f2;
  justify-content: center;
  align-items: center;
`;

export const Box = styled.View.attrs((props) => ({
  width: props.width,
  height: props.height,
}))`
  width: ${(props) => props.width || 'auto'};
  height: ${(props) => props.height || 'auto'};
  background-color: palevioletred;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.Text`
  color: rebeccapurple;
  font-size: 16px;
  font-weight: bold;
`;
