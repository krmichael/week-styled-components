import React, { useEffect } from 'react';
import { Dimensions } from 'react-native';

import { Container, Box, Text } from './styles';
const { width, height } = Dimensions.get('window');

export default function App() {
  useEffect(() => {
    Dimensions.addEventListener('change', ({ window, screen }) => {
      console.log(
        `window > Width: ${window.width}\nHeight: ${height}\nscreen > Width: ${
          screen.width
        }\nHeight: ${screen.height}`,
      );
    });

    return () =>
      Dimensions.removeEventListener('change', ({ window, screen }) => {});
  }, []);

  return (
    <Container>
      <Box width={width} height={height}>
        <Text>Hello</Text>
      </Box>
    </Container>
  );
}
