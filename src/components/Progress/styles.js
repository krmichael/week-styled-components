import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #f2f2f2;
  padding-left: 10px;
  padding-right: 10px;
`;

export const Progress = styled.ProgressBarAndroid`
  width: 100%;
`;
