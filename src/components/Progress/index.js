import React from 'react';

import { Container, Progress } from './styles';

export default function App() {
  return (
    <Container>
      <Progress styleAttr="Horizontal" color="#0099FF" indeterminate={true} />
    </Container>
  );
}
