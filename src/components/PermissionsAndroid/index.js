import React from 'react';

import { requestCameraPermission, checkPermission } from '../../utils';
import RippleButton from '../RippleButton';
import { Container } from './styles';

export default function App() {
  return (
    <Container>
      <RippleButton
        label="request camera permission"
        onPress={requestCameraPermission}
      />

      <RippleButton label="check permission camera" onPress={checkPermission} />
    </Container>
  );
}
