import styled from 'styled-components/native';
import { Platform } from 'react-native';

export const Container = styled.KeyboardAvoidingView.attrs({
  behavior: Platform.OS === 'ios' && 'padding',
})`
  flex: 1;
  background-color: #f2f2f2;
  justify-content: center;
  align-items: center;
  padding-left: 10px;
  padding-right: 10px;
`;

export const Input = styled.TextInput`
  align-self: stretch;
  border-bottom-width: 1px;
  border-bottom-color: #ee82ee;
`;

export const Button = styled.TouchableOpacity`
  align-self: stretch;
  padding-left: 10px;
  padding-right: 10px;
`;

export const TextButton = styled.Text`
  color: rebeccapurple;
  background-color: #fff;
  padding: 10px;
  border-radius: 5px;
  text-align: center;
  font-size: 16px;
  font-weight: bold;
  letter-spacing: 1;
  margin-top: 20px;
`;
