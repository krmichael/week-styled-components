import React from 'react';

import { Container, Input, Button, TextButton } from './styles';

export default class App extends React.Component {
  state = {
    name: '',
  };

  clearText = () => {
    this._inputText.setNativeProps({ text: '' });
  };

  render() {
    return (
      <Container>
        <Input
          ref={(elem) => (this._inputText = elem)}
          autoFocus
          autoCapitalize="none"
          autoCorrect={false}
          value={this.state.name}
          onChangeText={(name) => this.setState({ name })}
        />

        <Button onPress={this.clearText}>
          <TextButton>Clear</TextButton>
        </Button>
      </Container>
    );
  }
}
