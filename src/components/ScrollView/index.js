import React, { useState, useCallback, useEffect, useRef } from 'react';
import { Container, Scroll, Card, Text } from './styles';
import { RefreshControl } from 'react-native';

// @refresh rest
export default function App() {
  const [refreshing, setRefreshing] = useState(false);
  const [unread, setUnread] = useState(true);

  const scroll = useRef();

  function delay(timeout) {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  }

  useEffect(() => {
    scroll.current.scrollTo(); //scroll to top
    //scroll.current.scrollToEnd(); // scroll to bottom

    delay(2500).then(() => setUnread(false));
  }, []);

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    delay(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <Container>
      <Scroll
        ref={scroll}
        onMomentumScrollBegin={() =>
          console.log('O usuário esta fazendo scroll...')
        }
        onMomentumScrollEnd={() =>
          console.log('O usuário parou de fazer scroll...')
        }
        onScrollBeginDrag={() =>
          console.log('O usuário começou arrastar a exibição')
        }
        onScrollEndDrag={() =>
          console.log('O usuário parou de arrastar a exibição')
        }
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            progressBackgroundColor="#fff"
            colors={['#0099ff']}
          />
        }
        showsVerticalScrollIndicator={false}
        //stickyHeaderIndices={[0]} //Fixa o primeiro items do scroll no top ao rolar
        //persistentScrollbar={true} //sempre mostrando a barra de rolagem
        onContentSizeChange={(width, height) =>
          console.log(
            `A largura e altura do conteudo do scrollView mudou > Width ${width}, Height ${height}`,
          )
        }>
        <Card unread={unread} />

        <Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum
        </Text>

        <Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum
        </Text>

        <Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum
        </Text>

        <Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum
        </Text>
      </Scroll>
    </Container>
  );
}
