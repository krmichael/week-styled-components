import styled from 'styled-components/native';
import { StyleSheet } from 'react-native';

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #fff;
`;

export const Scroll = styled.ScrollView.attrs({
  contentContainerStyle: {
    paddingHorizontal: 20,
    paddingTop: 10,
  },
})``;

export const Card = styled.View`
  background-color: rebeccapurple;
  ${(props) => props.unread && { ...StyleSheet.absoluteFill }}
`;

export const Text = styled.Text`
  font-size: 18px;
`;
