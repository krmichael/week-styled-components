import React, { useState, useEffect } from 'react';
import { Picker, StatusBar, Animated, Text } from 'react-native';

import {
  Container,
  Scope,
  Button,
  ButtonText,
  ButtonTextCancel,
  RotateBox,
} from './styles';

export default function PickerComponent() {
  const ITEMS = [
    { id: 1, label: 'Javascript', value: 'Javascript' },
    { id: 2, label: 'React', value: 'React' },
    { id: 3, label: 'React Native', value: 'React Native' },
    { id: 4, label: 'Nodejs', value: 'Nodejs' },
  ];

  const [language, setLanguage] = useState(ITEMS[0].value);

  //Animation settings
  const animation = new Animated.Value(0);
  const rotation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  useEffect(() => {
    Animated.loop(
      Animated.timing(animation, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true,
      }),
    ).start();

    return () => {};
  }, [animation]);

  return (
    <Container>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <Picker
        selectedValue={language}
        onValueChange={(item) => setLanguage(item)}>
        {ITEMS.length &&
          ITEMS.map((item) => (
            <Picker.Item key={item.id} label={item.label} value={item.value} />
          ))}
      </Picker>

      <Scope>
        <Button activeOpacity={0.7}>
          <ButtonText color="rebeccapurple">Enviar</ButtonText>
        </Button>

        <Button activeOpacity={0.7}>
          <ButtonTextCancel>Cancelar</ButtonTextCancel>
        </Button>
      </Scope>

      <RotateBox style={{ transform: [{ rotate: rotation }] }}>
        <Text>&lt; 💅 &gt;</Text>
      </RotateBox>
    </Container>
  );
}
