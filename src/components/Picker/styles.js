import styled from 'styled-components/native';
import { Animated } from 'react-native';

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
`;

export const Scope = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
`;

export const Button = styled.TouchableOpacity`
  padding-left: 10px;
  padding-right: 10px;
`;

export const ButtonText = styled.Text.attrs((props) => ({
  color: props.color || 'palevioletred',
}))`
  background-color: papayawhip;
  color: ${(props) => props.color};
  border-width: 1px;
  border-color: papayawhip;
  padding: 7px 15px;
  font-size: 16;
  font-weight: bold;
  border-radius: 3px;
`;

export const ButtonTextCancel = styled(ButtonText)`
  letter-spacing: 1;
`;

export const RotateBox = styled(Animated.View)`
  margin-top: 50px;
  align-items: center;
`;
