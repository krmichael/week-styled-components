import styled, { css } from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #f2f2f2;

  ${(props) =>
    props.loading &&
    css`
      justify-content: center;
      align-items: center;
    `}
`;

export const Loading = styled.ActivityIndicator.attrs({
  color: '#f44336',
  size: 'large',
})``;

export const Content = styled.View``;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})``;

export const Title = styled.Text`
  color: palevioletred;
  text-align: center;
  margin: 10px;
  font-size: 18px;
  font-weight: bold;
`;

export const Card = styled.View.attrs({
  elevation: 1,
})`
  /*padding: 20px;*/
  height: 60px;
  padding-left: 20px;
  justify-content: center;
  background-color: #fff;
  margin: 5px 10px;
  border-radius: 3px;
`;

export const CardItem = styled.Text.attrs({
  numberOfLines: 1,
})`
  color: rebeccapurple;
  font-size: 16px;
`;
