import React, { Component } from 'react';

import {
  Container,
  Loading,
  Content,
  List,
  Title,
  Card,
  CardItem,
} from './styles';

import { wait } from '../../utils';
import posters from '../../data/data.json';

// @refresh reset
function renderItem({ item }) {
  return (
    <Card>
      <CardItem>{item['Autor(a) apresentador(a)']}</CardItem>
    </Card>
  );
}

export default class App extends Component {
  state = {
    data: [],
    loading: true,
  };

  fetch = () => {
    const dataObj = Object.keys(posters)
      .map((key) => {
        return posters[key];
      })
      .reduce((acc, cur) => acc.concat(cur), []);

    this.setState({ data: dataObj });
  };

  componentDidMount() {
    this.fetch();

    wait(2000).then(() => {
      this.setState({ loading: false });
    });
  }

  render() {
    const { loading, data } = this.state;
    const ITEM_HEIGHT = 70;

    return (
      <Container loading={loading}>
        {loading ? (
          <Loading />
        ) : (
          <Content>
            <Title>Palestrantes</Title>

            <List
              data={data}
              renderItem={renderItem}
              windowSize={11}
              getItemLayout={(data, index) => ({
                offset: ITEM_HEIGHT * index,
                length: ITEM_HEIGHT,
                index,
              })}
              keyExtractor={(item, index) => item.ID + index}
            />
          </Content>
        )}
      </Container>
    );
  }
}
