import React, { useState } from 'react';
import { Platform, UIManager, LayoutAnimation } from 'react-native';

import { Container, Button, ButtonText, Message } from './styles';

export default function App() {
  const [expanded, setExpanded] = useState(false);

  if (
    Platform.OS === 'android' &&
    UIManager.setLayoutAnimationEnabledExperimental
  ) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  return (
    <Container>
      <Button
        onPress={() => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
          setExpanded(!expanded);
        }}>
        <ButtonText>Press me to {expanded ? 'colapse' : 'expand'}</ButtonText>
      </Button>

      {expanded && <Message>I disappear sometimes!</Message>}
    </Container>
  );
}
