import styled from 'styled-components/native';

export const Container = styled.View`
  overflow: hidden;
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #f2f2f2;
`;

export const Button = styled.TouchableOpacity``;

export const ButtonText = styled.Text`
  color: rebeccapurple;
  font-size: 18px;
  font-weight: bold;
`;

export const Message = styled.Text`
  margin-top: 20px;
  color: palevioletred;
  font-size: 16px;
  font-weight: bold;
`;
