import React from 'react';
import { ToastAndroid } from 'react-native';

import RippleButton from '../RippleButton';
import { Container } from './styles';

export default function ToastComponent() {
  return (
    <Container>
      <RippleButton
        label="Show toast"
        onPress={() =>
          ToastAndroid.showWithGravity(
            'React native is cool',
            ToastAndroid.LONG,
            ToastAndroid.CENTER,
          )
        }
      />
    </Container>
  );
}
