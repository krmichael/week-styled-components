import React from 'react';
import { StatusBar } from 'react-native';

import { Container, Touch, Hello } from './styles';

export default function View() {
  return (
    <Container>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />

      <Touch
        onMoveShouldSetResponderCapture={(evt) => {
          console.log(
            'Retornando true para impedir que a view filha se torne respondedora de toques [onMoveShouldSetResponderCapture]',
          );
          return true;
        }}
        accessibilityHint="Clicar para mostrar os eventos no console"
        accessibilityLabel="Botão Hello"
        accessible={true}
        onAccessibilityTap={() =>
          console.log('O usuário executou um gesto de acessibilidade')
        }
        onLayout={({
          nativeEvent: {
            layout: { x, y },
          },
        }) => {
          console.log(`X: ${x}\nY: ${y}`);
        }}
        onMagicTap={() =>
          console.log(
            'O usuário executou um gesto de toque mágico [onMagicTap]',
          )
        }
        onMoveShouldSetResponder={(event) => {
          //console.log(event);
        }}
        onTouchMove={({ nativeEvent }) => {
          /* console.log(
            'O usuário está movendo o dedo sobre a tela',
            nativeEvent,
          ); */
        }}
        onResponderGrant={(evt) =>
          console.log(
            'A view agora está respondendo a eventos de toques [onResponderGrant]',
          )
        }
        onResponderMove={(evt) =>
          console.log(
            'O usuário está movendo o dedo sobre a tela [onResponderMove]',
          )
        }
        onResponderReject={(evt) =>
          console.log(
            'Um respondedor de eventos já está ativo e não liberará para o pedido do evento atual [onResponderReject]',
          )
        }
        onResponderRelease={(evt) => {
          console.log(
            'Evento disparado no final do toque [onResponderRelease]',
          );
        }}
        onResponderTerminate={(evt) =>
          console.log(
            'O respondedor de eventos foi retirado da View [onResponderTerminate]',
          )
        }
        onStartShouldSetResponderCapture={() => {
          console.log(
            'Retornando true para impedir que a view filha se torne respondedora rápidamente [onStartShouldSetResponderCapture]',
          );
        }}
        pointerEvents="auto" //Controla se a view pode ter eventos de toque, default is auto
        nativeID="hellobutton">
        <Hello>Hello</Hello>
      </Touch>
    </Container>
  );
}
