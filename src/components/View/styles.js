import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

export const Touch = styled.View`
  padding: 7px 30px;
  align-items: center;
  justify-content: center;
  border: 1px solid #ccc;
`;

export const Hello = styled.Text`
  color: rebeccapurple;
  font-size: 18;
  font-weight: bold;
  letter-spacing: 1;
`;
