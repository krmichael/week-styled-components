import styled from 'styled-components/native';
import ViewPager from '@react-native-community/viewpager';

export const Pager = styled(ViewPager)`
  flex: 1;
`;

export const Scene = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const SceneContent = styled.Text`
  color: palevioletred;
  font-size: 16px;
  font-weight: bold;
`;
