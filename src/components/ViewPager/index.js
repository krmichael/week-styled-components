import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Pager from '@react-native-community/viewpager';

// Show message warning
// Each ViewPager child must be a <View>. Was Styled(RCTView)
//when usage with styled components

export default function App() {
  const { container, scene, sceneContent } = styles;

  return (
    <Pager style={container} initialPage={0}>
      <View key="1" style={scene}>
        <Text style={sceneContent}>First Page</Text>
      </View>

      <View key="2" style={scene}>
        <Text style={sceneContent}>Second Page</Text>
      </View>
    </Pager>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  scene: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sceneContent: {
    color: 'palevioletred',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
