import React from 'react';
import { ToolbarAndroid as Toolbar } from 'react-native';

import logo from '../../assets/avatar.png';
import { Container } from './styles';

export default function App() {
  function onActionSelected(position) {
    if (position === 0) {
      // Call settings component
    }
  }

  return (
    <Container>
      <Toolbar
        logo={logo}
        title="Home"
        actions={[
          {
            title: 'Settings',
            icon: logo,
            show: 'always',
          },
        ]}
        onActionSelected={onActionSelected}
      />
    </Container>
  );
}
