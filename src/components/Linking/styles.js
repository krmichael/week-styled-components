import styled from 'styled-components/native';
import { Platform, TouchableNativeFeedback } from 'react-native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #f2f2f2;
`;

export const ScopeButton = styled.View`
  width: 250px;
  background-color: white;
  height: 50px;
  justify-content: center;
  align-items: center;
  border-radius: 3px;
`;

export const Button = styled.TouchableNativeFeedback.attrs({
  background:
    Platform.Version >= 21
      ? TouchableNativeFeedback.Ripple('palevioletred')
      : TouchableNativeFeedback.SelectableBackground(),
  useForeground: TouchableNativeFeedback.canUseNativeForeground(),
})``;

export const ButtonView = styled.View`
  width: 250px;
  height: 50px;
  justify-content: center;
  align-items: center;
`;

export const ButtonText = styled.Text`
  color: palevioletred;
  font-size: 16px;
  font-weight: bold;
`;

export const Message = styled.Text`
  color: red;
  margin-top: 20px;
  font-size: 14px;
  letter-spacing: 1;
  text-align: center;
`;
