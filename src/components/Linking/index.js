import React, { useState } from 'react';
//import { Linking } from 'react-native';

import { verifyCanOpenUrl } from '../../utils';

import {
  Container,
  ScopeButton,
  Button,
  ButtonView,
  ButtonText,
  Message,
} from './styles';

export default function App() {
  const [error, setError] = useState(null);

  return (
    <Container>
      <ScopeButton>
        <Button
          onPress={() =>
            verifyCanOpenUrl('https://leansilva.gitlab.io').catch((err) => {
              err && setError(err);
            })
          }
          //onPress={() => Linking.openSettings()} // Abre as configs do proprio app
          disabled={error && true}>
          <ButtonView>
            <ButtonText>Press me now!</ButtonText>
          </ButtonView>
        </Button>
      </ScopeButton>

      {error && <Message>{error.message}</Message>}
    </Container>
  );
}
