import { TouchableNativeFeedback, Platform } from 'react-native';
import styled from 'styled-components/native';

export const Button = styled.TouchableNativeFeedback.attrs({
  background:
    Platform.Version >= 21
      ? TouchableNativeFeedback.Ripple('#fff')
      : TouchableNativeFeedback.SelectableBackground(),
  useForeground: TouchableNativeFeedback.canUseNativeForeground(),
})``;

export const ButtonView = styled.View`
  min-width: 250px;
  height: 50px;
  justify-content: center;
  align-items: center;
  padding: 10px;
  border-radius: 3px;
`;

export const ScopeButton = styled(ButtonView)`
  padding: 0;
  background-color: palevioletred;
  border: 2px solid #fff;
  margin-bottom: 20px;
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 18px;
  letter-spacing: 1;
`;
