import React from 'react';

import { ScopeButton, Button, ButtonView, ButtonText } from './styles';

export default function RippleButton({ label, ...props }) {
  return (
    <ScopeButton>
      <Button {...props}>
        <ButtonView>
          <ButtonText>{label}</ButtonText>
        </ButtonView>
      </Button>
    </ScopeButton>
  );
}
