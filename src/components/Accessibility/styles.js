import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #f2f2f2;
`;

export const Button = styled.TouchableOpacity``;

export const ButtonText = styled.Text`
  color: rebeccapurple;
  font-size: 16px;
  font-weight: bold;
  padding: 12px 30px;
  background-color: #fff;
  border-radius: 3px;
`;
