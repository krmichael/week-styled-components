import React from 'react';
import { Container, Button, ButtonText } from './styles';

export default function App() {
  return (
    <Container>
      <Button
        activeOpacity={0.5}
        accessible={true}
        accessibilityLabel="Go Back"
        accessibilityHint="Navigate to the previous screen"
        accessibilityIgnoresInvertColors={false}
        accessibilityRole="button"
        accessibilityStates={['unchecked']}>
        <ButtonText>Back</ButtonText>
      </Button>
    </Container>
  );
}
