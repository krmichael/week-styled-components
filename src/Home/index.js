import React, { Fragment } from 'react';
import { StatusBar } from 'react-native';

import Icon from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import FaIcon from 'react-native-vector-icons/FontAwesome';

import {
  Container,
  Location,
  LocationText,
  Section,
  User,
  ScopeButtons,
  Button,
  Hello,
  Footer,
  FooterIcon,
} from './styles';

import Avatar from '../assets/avatar2.png';

export default function Home() {
  return (
    <Fragment>
      <StatusBar
        backgroundColor={'transparent'}
        barStyle={'light-content'}
        translucent={true}
      />

      <Container source={Avatar}>
        <Location>
          <Icon name="location-pin" size={24} color="#0099FF" />
          <LocationText>Nova York - NY</LocationText>
        </Location>

        <Section>
          <User>Scarlett Johansson, 27</User>

          <ScopeButtons>
            <Button>
              <MaterialIcon name="close" size={30} color="#bbb" />
            </Button>

            <Button>
              <MaterialIcon name="star" size={30} color="#f4d909" />
              <Hello>Hello</Hello>
            </Button>

            <Button>
              <MaterialCommunityIcon name="heart" size={30} color="#e83336" />
            </Button>
          </ScopeButtons>

          <Footer>
            <FooterIcon>
              <MaterialCommunityIcon
                name="home-map-marker"
                size={27}
                color="#fff"
              />
            </FooterIcon>

            <FooterIcon>
              <MaterialCommunityIcon name="bell" size={27} color="#bbb" />
            </FooterIcon>

            <FooterIcon>
              <Icon name="globe" size={27} color="#bbb" />
            </FooterIcon>

            <FooterIcon>
              <MaterialIcon name="message" size={27} color="#bbb" />
            </FooterIcon>

            <FooterIcon>
              <FaIcon name="user" size={27} color="#bbb" />
            </FooterIcon>
          </Footer>
        </Section>
      </Container>
    </Fragment>
  );
}
