import styled from 'styled-components/native';
import { StyleSheet } from 'react-native';

export const Container = styled.ImageBackground`
  flex: 1;
  width: 100%;
  height: 100%;
`;

export const Location = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
  margin-top: 50px;
  margin-left: 10px;
`;

export const LocationText = styled.Text`
  font-size: 12;
  font-weight: bold;
  color: #fff;
  margin-left: 3px;
  margin-top: 3px;
  text-transform: uppercase;
`;

export const Section = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;

export const User = styled.Text`
  font-size: 18;
  font-weight: bold;
  color: #fff;
  margin-bottom: 15px;
`;

export const ScopeButtons = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  margin-bottom: 15px;
`;

export const Button = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: #fff;
  border-radius: 50px;
  margin-top: 10px;
  padding: 10px;
`;

export const Hello = styled.Text`
  color: #0099ff;
  font-size: 15;
  font-weight: bold;
  text-transform: uppercase;
  margin-left: 5px;
  padding-right: 10px;
`;

export const Footer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  margin-top: 10px;
  border-top-width: ${StyleSheet.hairlineWidth}px;
  border-top-color: #bbb;
`;

export const FooterIcon = styled.View`
  padding: 10px;
  margin-top: 5px;
  margin-bottom: 5px;
  justify-content: center;
  align-items: center;
`;
